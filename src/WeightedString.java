import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WeightedString {

    private static final String EMPTY_STRING = "";
    private static final String YES = "Yes";
    private static final String NO = "No";

    public static void main(String[] args) throws Exception {
        validateWeight(null, List.of(1,3,9,8)); // INVALID INPUT
        validateWeight("", List.of(1,3,9,8)); // INVALID INPUT
        validateWeight("  12345  ", List.of(1,3,9,8)); // INVALID INPUT
        validateWeight("aabbcccd", List.of(1,3,9,8)); // [Yes, Yes, Yes, No]
        validateWeight("jkklmnoo0", List.of(1,22,10,45)); // [No, Yes, Yes, No]
        validateWeight("zyxxj", List.of(25,24,48,45)); // [Yes, Yes, Yes, Yes]
    }

    public static void validateWeight(String input, List<Integer> queries) {
        String validString = validateString(input);
        if (validString.isBlank()) {
            System.out.println("INVALID INPUT");
            return;
        }
        System.out.println("=======================");
        System.out.println("input = " + validString);
        List<Integer> values = sumCharValue(validString);
        System.out.println("calculate total value " + String.join("+", values.stream().map(x -> x.toString()).collect(Collectors.toList())) + "="
                + values.stream().reduce(0, Integer::sum));
        Map<String, Integer> weights = calculateWeight(groupKey(validString));
        System.out.println(weights);

        System.out.println("===== validate result =====");
        List<String> result = new ArrayList<>();
        for (Integer query : queries) {
            result.add(weights.values().stream().anyMatch(integer -> integer == query) ? YES : NO);
        }
        System.out.println(queries);
        System.out.println(result);
        System.out.println();
    }

    public static Map<Character, Integer> groupKey(String validString) {
        Map<Character, Integer> groupedKey = new LinkedHashMap<>();
        List<Character> keys = validString.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        for (Character key : keys.stream().collect(Collectors.toSet())) {
            groupedKey.put(key, (int) keys.stream().filter(x -> x.equals(key)).count());
        }
        return groupedKey;
    }

    public static Map<String, Integer> calculateWeight(Map<Character, Integer> groupedKey) {
        Map<String, Integer> result = new LinkedHashMap<>();
        for (var each : groupedKey.entrySet()) {
            int weight = charToNumber(each.getKey());
            for (int i = 1; i <= each.getValue(); i++) {
                String key = EMPTY_STRING;
                for (int j = 1; j <= i; j++) {
                    key += each.getKey();
                }
                result.put(key, weight * i);
            }
        }
        return result;
    }

    public static List<Integer> sumCharValue(String input) {
        char[] charArray = input.toCharArray();
        List<Integer> values = new ArrayList<>();
        for (char c : charArray) {
            values.add(charToNumber(c));
        }
        return values;
    }

    public static int charToNumber(char c) {
        return c - 'a' + 1;
    }

    public static String validateString(String str) {
        if (str == null) return EMPTY_STRING;
        return str.toLowerCase().replaceAll("[^a-zA-Z]", EMPTY_STRING);
    }
}