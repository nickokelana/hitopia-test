import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BalancedBracket {
    private static final String EMPTY_STRING = "";
    private static final String YES = "YES";
    private static final String NO = "NO";

    public static void main(String[] args) {
        System.out.println(validateBracket("({asd})[](123)")); // TRUE
        System.out.println();
        System.out.println(validateBracket("    ({})[    ]()  ")); // TRUE
        System.out.println();
        System.out.println(validateBracket("{({''})[]{(123)}}")); // TRUE
        System.out.println();
        System.out.println(validateBracket("}lorem[]")); // FALSE
        System.out.println();
        System.out.println(validateBracket("())(")); // FALSE
    }

    public static String validateBracket(String input) {
        Map<String, String> allowedBracket = new HashMap<>();
        allowedBracket.put("{", "}");
        allowedBracket.put("(", ")");
        allowedBracket.put("[", "]");

        System.out.println("INPUT = \"" + input + "\"");
        String validString = validateString(input);
        System.out.println("BRACKET = " + validString);
        if (validString.isBlank()) return YES;
        char[] chars = validString.toCharArray();
        List<String> needCloseBracket = new ArrayList<>();
        for (char c : chars) {
            String bracket = String.valueOf(c);
            if (allowedBracket.keySet().stream().anyMatch(bracket::equalsIgnoreCase)) {
                needCloseBracket.add(allowedBracket.get(bracket));
                continue;
            }
            if (allowedBracket.values().stream().anyMatch(bracket::equalsIgnoreCase)) {
                if (needCloseBracket.isEmpty()) return NO;
                String lastNeedCloseBracket = needCloseBracket.get(needCloseBracket.size() - 1);
                if (bracket.equalsIgnoreCase(lastNeedCloseBracket)) {
                    needCloseBracket.remove(needCloseBracket.size() - 1);
                } else return NO;
            }
        }
        return needCloseBracket.isEmpty() ? YES : NO;
    }

    public static String validateString(String str) {
        if (str == null) return EMPTY_STRING;
        return str.toLowerCase().replaceAll("[^{}()\\[\\]]", EMPTY_STRING);
    }
}