import java.util.*;
import java.util.regex.Pattern;

public class HighestPalindrome {
    private static final String INVALID = "-1";

    public static void main(String[] args) {
        calculateHighestPalindrome(null, 1); // -1
        calculateHighestPalindrome(" ", 1); // -1
        calculateHighestPalindrome("3943", 1); // 3993
        calculateHighestPalindrome("932239", 2); // 992299
        calculateHighestPalindrome("  9872652  ", 1); // 98722789
        calculateHighestPalindrome("  987265298  ", 3); // 999222999
        calculateHighestPalindrome("  12345  ", 1); // 54345
        calculateHighestPalindrome("  12345  ", 3); // -1
    }

    public static String calculateHighestPalindrome(String s, int k) {
        System.out.println("============");
        System.out.println("input:");
        System.out.println("s:" + s);
        System.out.println("k:" + k);
        if (!validateString(s)) {
            System.out.println("output: " + INVALID);
            return INVALID;
        }
        String validString = s.trim();
        if (validString.length() % k > 0) {
            System.out.println("output: " + INVALID);
            return INVALID;
        }
        boolean isOdd = validString.length() % 2 == 1;
        List<String> patterns = findPattern(validString, isOdd);
        System.out.println("palindrome:");
        List<String> palindromes = generatePalindromes(new ArrayList<>(), patterns, k, isOdd, 0);
        String max = findMaxPalindrome(validString, palindromes, 0);
        System.out.println("output: " + max);
        return max;
    }

    public static List<String> findPattern(String validString, boolean isOdd) {
        Set<String> pattern = new HashSet<>();
        pattern.add(validString.substring(0, validString.length() / 2 + (isOdd ? 1 : 0)));
        pattern.add(reverseString("", validString.substring((validString.length() / 2)), 0));
        return new ArrayList<>(pattern);
    }

    public static String replaceString(String palindrome, String pattern, int k, int index) {
        if (index >= pattern.length()) return palindrome.substring(0, pattern.length());
        palindrome += String.valueOf(pattern.charAt(index)).repeat(k);
        return replaceString(palindrome, pattern, k, index + k);
    }

    public static List<String> generatePalindromes(List<String> palindromes, List<String> patterns, int k, boolean isOdd, int index) {
        if (patterns.size() == index) return palindromes;
        String replacedPattern = replaceString("", patterns.get(index), k, 0);
        String palindrome = replacedPattern + reverseString("", replacedPattern, 0).substring(isOdd ? 1 : 0);
        System.out.println((index + 1) + ". " + palindrome);
        palindromes.add(palindrome);
        return generatePalindromes(palindromes, patterns, k, isOdd, index + 1);
    }

    public static String findMaxPalindrome(String maxValue, List<String> palindromes, int index) {
        if (palindromes.size() == index) return maxValue;
        if (Integer.valueOf(maxValue) < Integer.valueOf(palindromes.get(index))) maxValue = palindromes.get(index);
        return findMaxPalindrome(maxValue, palindromes, index + 1);
    }

    public static String reverseString(String str, String res, int startIndex) {
        if (startIndex == res.length()) return str;
        str = res.charAt(startIndex) + str;
        return reverseString(str, res, startIndex + 1);
    }

    public static boolean validateString(String str) {
        if (str == null || str.isBlank()) return false;
        return Pattern.compile("[0-9]+").matcher(str.trim()).matches();
    }
}
